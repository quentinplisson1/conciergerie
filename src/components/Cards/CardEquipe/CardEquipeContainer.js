import React, { Component } from 'react';
import CardEquipe from './CardEquipe';

class CardEquipeContainer extends Component {
    render() {
        return (
            <CardEquipe
                data={this.props.data}
                nom={this.props.nom}
                prenom={this.props.prenom}
                profession={this.props.profession}
                photo={this.props.photo}
            />
        );
    }
}

export default CardEquipeContainer;
