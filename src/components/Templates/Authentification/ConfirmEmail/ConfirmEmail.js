import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import Header from '../../../Header/HeaderContainer';
import Footer from '../../../Footer/FooterContainer';
import Styles from './Styles';
import DelayedRedirect from '../../../DelayedRedirect';

class ConfirmEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmed:false,
            error: ""
        };
    }

    componentDidMount(){
        window.scrollTo(0, 0);
    }

    render() {        
        return (
            <div className="row">
            <Helmet>
                <title>Confirmation de l'ouverture du compte || La Conciergeries des Petits</title>
                <meta name="keywords" content="HTML,CSS,JavaScript"/>
                <meta name="author" content="Adrien Gillmann"/>
                <meta
                    name="description"
                    content="Confirmation de l'ouverture du compte"  
                />
            </Helmet>
            <Header/>
            <div className="col-xs-12">
                <Styles className="row" style={{marginBottom: 50}}>
                    <h1 className="col-xs-12 center-xs"style={{marginBottom: 200, marginTop: 370}}>Votre compte a bien été confirmé, vous allez être redirigé dans quelques secondes</h1>
                    <DelayedRedirect to="/profile" delay={5000}/>
                </Styles>
            </div>
            <Footer/>
            </div>
        );
        }
}

export default ConfirmEmail;