import React from 'react';
import ReactDOM from 'react-dom';
import Router from './components/Router';
import 'normalize.css';
import 'flexboxgrid';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Router/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
