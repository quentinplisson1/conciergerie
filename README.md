# React app La Conciergerie des Petits

## Prérequis 

Installer git, node ou yarn sur votre machine 

* [node](https://nodejs.org/en/)
* [yarn](https://yarnpkg.com/lang/en/)

## Installer dependance & lancement en local
  
*  Télécharger  le repot

`git clone https://gitlab.com/Agillmann/conciergerie.git front-end`

* Avec npm :

`npm install`

`npm start`

* Avec yarn

`yarn`

`yarn start`

Dans le navigateur vous pouvez maintenant accèder a l'app en local sur le port 3000 [http://localhost:3000](http://localhost:3000/)

## Local

Utilise [strapi-conciergerie](https://gitlab.com/Agillmann/strapi-conciergerie) une API pour stocké les services et les clients avec un systeme d'authentification
(Voir doc du projet)

Changer le fichier [api.js](https://gitlab.com/Agillmann/conciergerie/blob/master/src/helpers/api.js) pour utiliser les urls strapi local.

Variable d'environnement :

*  SERVICE_ENFANCE_URL 

*  SERVICE_PETITE_ENFANCE_URL

*  SERVICE_PARENT_URL

*  SERVICE_URL = "http://localhost:1337/services/";

*  REGISTER_URL = "http://localhost:1337/auth/local/register";

*  LOGIN_URL = "http://localhost:1337/auth/local";

*  INFOCLIENT_URL = "http://localhost:1337/infoclients/";

## Production 

Deploiement avec heroku : 

Installation

*  Debian/Ubuntu

`$ wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh`

*  MacOS

`$ brew install heroku/brew/heroku`

*  Login 

`heroku login`

... 

`npm run build`

`npm start-prod`

## Author & Licences

Adrien Gillmann
